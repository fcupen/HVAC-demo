'use strict';

angular.module('myApp.home', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/home', { 
    templateUrl: 'home/home.html',
    controller: 'HomeCtrl'
  }); 
}])

.controller('HomeCtrl', function($scope ,$http ,$rootScope, $location) {

	
    $rootScope.active = 'home'; 

	
    var _selected; 

    $scope.selected = undefined;

    $scope.ngModelOptionsSelected = function(value) {
        if (arguments.length) {
            _selected = value;
        } else {
            return _selected;
        }
    };

    $scope.modelOptions = {
        debounce: {
            default: 500, 
            blur: 250
        },
        getterSetter: true
    }; 
	//----
	
	if($rootScope.d != ''){
	$rootScope.dess = function () {
            $http.get("/rhAPI/designers/"+$rootScope.c).then(function (response) {
				
                $scope.data.designers = response.data.Designers;
            });
		};};
	if($rootScope.c){
				$rootScope.dess();}
				if($rootScope.c != ''){
    if(!$rootScope.dashboard) {
        $http.get("/rhAPI/contractors").then(function (response) {
            $scope.data.contractors = response.data.Contractors;
			$scope.aux=$scope.data.contractors;
			if($rootScope.c){
				$rootScope.dess();}
        });
        
    };}
	
});
