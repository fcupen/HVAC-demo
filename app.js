'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'ngAnimate',
    'ui.bootstrap',
    'myApp.home',
    'myApp.designs',
	'myApp.viewdesigns',
    'myApp.newDesign',
    'myApp.systems',
    'myApp.newSystem',
    'myApp.locations',
    'myApp.help',
    'myApp.version',
	'myApp.editDesign',
	'myApp.editSystems',
	'myApp.newEquipment',
	'myApp.equipment',
	'myApp.newLocation',
	'myApp.copyDesign',
	'myApp.editEquipment',
	'myApp.designsystems'
	
]).
config(['$routeProvider', function($routeProvider) {
    $routeProvider.otherwise({redirectTo: '/home'});
}]).
controller('rhCtrl',function($scope, $http ,$rootScope){
	
    $scope.data = {
        active           : 'home',
        allDesigns       : false,
        allEquipment     : false,
        counties         : [],
        dashboard        : false,
        designs          : [],
		viewdesigns      : [],
        design           : {
            areaserved          : "Select: ",
            tempoccload         : "Select: ",
            ventmeet622         : "Select: ",
            systtypesp          : "Select: ",
            opwointrvntn        : "Select: ",
            ventoverride        : "Select: ",
            useintakeright      : "Select: ",
            fanratedsones       : "Select: ",
            fanreducevent       : "Select: ",
            bathroomfanses      : "Select: ",
            inletpullsoutdr     : "Select: ",
            inletlocation       : "Select: ",
            heatlossmtd         : "Select: ",
            indoortemp          : "Select: ",
            outdoortemplocation : "Select: ",
            summerinfiltration  : "Select: ",
            winterinfiltration  : "Select: ",
            mansmethod          : "Select: "
        },
        equip            : {
            equipmentname: "Select Equipment:"
        },
        equipment        : [],
        filteredDesigns  : [],
        section          : 3,
        systems          : [],
        system           : {}
    };
	$scope.data.dashboard ='';
    $scope.workers = {
        dashboard: function(){
            if(($scope.contractor) && ($scope.designer)){
                $scope.data.dashboard = !$scope.data.dashboard;
				$rootScope.dashboard = !$scope.data.dashboard;
            }else{}
        },
        setActive : function(link){
            $rootScope.active = link;
			$scope.data.active = link;
        },
        setContractor : function(contractor){
            $scope.contractor = contractor;
			$rootScope.c=contractor.id;
			$rootScope.cn=contractor.name;
			$rootScope.dess();
			
        },
        setDesigner : function(designer){
            $scope.designer = designer;
			$rootScope.d=$scope.designer.id;
			$rootScope.dn=$scope.designer.name;
        },
        setScope : function(attribute, value){
            $scope.data[attribute] = value;
        },
        onTextClick : function ($event) {
            $event.target.select();
        },
        designsFilter : function(designs, designer, allDesigns){
            var filteredDesigns = [];
            if(designer && !allDesigns) {
                for (var i = 0; i < designs.length; i++) {
                    if(designs[i].contact_id == designer.id) {
                        filteredDesigns.push(designs[i]);
                    }
                }
            }else if(designer && allDesigns){
                filteredDesigns = $scope.data.designs;
            }else{
            }
            return filteredDesigns;
        },
        allDesigns : function() {
            $scope.data.allDesigns = !$scope.data.allDesigns;
            $scope.data.filteredDesigns = $scope.workers.designsFilter($scope.data.designs, $scope.designer, $scope.data.allDesigns);
        },
        setSection : function(section){
            $rootScope.section = section;
			$scope.data.section = section;
        },
        setEquip : function(value){
            $scope.data.equip = value;
        },
        setDesignField : function(attribute, value){
            $scope.data.design[attribute] = value;
			$rootScope.design[attribute] = value;
        },
        getCounties : function(){
            $scope.workers.setSection(3);
            $http.get("/rhAPI/counties").then(function(response){
                $scope.data.counties = response.data.Counties;
            })
        },
        getEquipment : function(){
            $scope.workers.setSection(4);
            if($scope.contractor) {
                if ($scope.contractor.id && !$scope.data.allEquipment) {
                    $http.get("/rhAPI/getEquipment?q=" + $scope.contractor.id).then(function (response) {
                        
                        $scope.data.equipment = response.data.Equipment;
                        
                    });
                } else {
                    $http.get("/rhAPI/getEquipment?q=all").then(function (response) {
                        $scope.data.equipment = response.data.Equipment;
                    });
                }
            }else {
            }
        }
    };
});
