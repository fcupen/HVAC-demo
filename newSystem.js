'use strict';

angular.module('myApp.newSystem', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/newSystem', {
    templateUrl: 'newSystem/designs.html',
    controller: 'NewSystemCtrl'
  });
  $routeProvider.when('/newSystem_dsg/:id', {
    templateUrl: 'newSystem/newSystem.html',
    controller: 'NewSystemCtrl'
  });
  $routeProvider.when('/newSystem_dsg_room/:id/:id_system', {
    templateUrl: 'newSystem/newSystem_room.html',
    controller: 'NewSystemCtrl'
  });
}])

.controller('NewSystemCtrl', function($scope ,$rootScope, $http,  $routeParams, $location) {
	if(!$scope.contractor){
	$location.path('#/');
	};
	$scope.edit=true;
	$scope.design={};
	$scope.c=$rootScope.c;
	$scope.d=$rootScope.d;
    $rootScope.active = 'newSystem';
	$scope.system = {};
	var id = $routeParams.id;
	var id_system = $routeParams.id_system;
	$scope.select_system_id = id_system;	
	$scope.select_design_id = id;
	$scope.system.hvac_design_id = id;
	$scope.design.hvac_design_id = id;
	var name = $routeParams.name;
	
	
	$http.get("/rhAPI/viewdesigns/"+id).then(function(response){

	$scope.des = response.data.Designs;
	$scope.select_design_name = $scope.des.hvac_dsn_descrpt;
	
	});
	
	$http.get("/rhAPI/designs/"+$scope.d+"/"+$scope.c).then(function(response){

	$scope.designs = response.data.Designs;
	var num=0;
	$scope.filtdesign = function(comp,design){
		if((comp == $scope.contractor.id)&&(design == $scope.designer.id)){
			num =1
			$scope.auu= true;
			return true;
		}
		if(num == 0){
			$scope.auu= false;
			return false;
		}
	}
	
			

	$scope.data.designs = response.data.Designs;
    $scope.data.filteredDesigns = $scope.workers.designsFilter($scope.data.designs, $scope.designer, $scope.data.allDesigns);
  });
  
  
    $scope.name = function(){	
  if($scope.system.name.toString().length >= 1){
   $http.get("/rhAPI/verifsystems/"+$scope.system.name).then(function(response){
    $scope.names=response.data.system;
	
    
  });
  }
  };
  $scope.seee=false;
  
  if( $rootScope.active == 'newSystem'){
  $http.get("/rhAPI/getEquipment/"+$scope.contractor.id).then(function (response) {
	  
            $scope.data.equipments = response.data.Equipment;
        });};
	var i,aux,completado;
	$http.get("/rhAPI/getSystemByDesignComplete/"+$scope.system.hvac_design_id).then(function(response){
    
	$scope.sys = response.data.system;
	$scope.data.systems = response.data.system;
	
	
	aux=$scope.sys.length;
	for (i = 0; i < $scope.sys.length;i++){
		
		if(($scope.sys[i].systemcomplete == 'true')||($scope.sys[i].systemcomplete == true)){
			completado++;
			
		}
	}
  });	
		
		if(aux==completado){
			
			$scope.system.completesystems = true;
		}else{
			$scope.system.completesystems = false;
		}
		
		
	$scope.select_equip = function(data){	

	
	
		$http.get("/rhAPI/getOneEquipment/"+data).then(function (response) {
            $scope.equipment = response.data.Equipment[0];
			$scope.system.equipment_id = $scope.equipment.equipment_id;
			
			
        }).then(function () {
		$scope.system.coolsizingpct= (($scope.equipment.totaldescap / $scope.system.maxtotal)*100).toFixed(2);
		$scope.system.heatsizepctlimits= (($scope.equipment.furnoutput / $scope.system.desttotlheatloss)*100).toFixed(2);
		$scope.seee=true;
		});
		
		
	}	
  
 
  var total=36;
  var req = 6
  var porc=(req/total)*100;
  var unit = (1/36)*100;
  var falt = ((total-req)/total)*100;
  
  var unit2 = 0;
 var hvac_design_id = 0, name = 0, radio1 = 0, ventairrate = 0, venairflo0 = 0, runtimecyc = 0, cycletimes = 0, specifiedsystype = 0, contlocspe = 0, opwointrvntn = 0, ventoverride = 0, useintakeright = 0, fanratedsones = 0, fanreducevnt = 0, bathroomfanses = 0, inletpullsoutdr = 0, inletlocation = 0, dessheatgainn = 0, dessheatgainne = 0, dessheatgaine = 0, dessheatgainse = 0, dessheatgains = 0, dessheatgainsw = 0, dessheatgainw = 0, dessheatgainnw = 0, maxheatgain = 0, deslheatgain = 0, destotlheatgainn = 0, destotlheatgainne = 0, destotlheatgainnw = 0, destotlheatgains = 0, destotlheatgainse = 0, destotlheatgainsw = 0, destotlheatgaine = 0, destotlheatgainw = 0, maxtotal = 0, desttotlheatloss = 0, mansmethod = 0, coolsizingpct = 0, coolingsizinglimit = 0, coolsizepctlimit = 0, heatsizinglimit = 0, heatsizepctlimit = 0, manuald = 0, designairflowcool = 0,designairflowheat = 0, hvacfanspdcool = 0, hvacfanspdheat = 0, designstaticp = 0, equipment_id =0, hrt =0, gfh=0;
 var llevo,llevo2,llevo3;
 //***
   $scope.prog = function(){	
   llevo=name+ radio1+ ventairrate+ venairflo0+ runtimecyc+ cycletimes+ specifiedsystype+ contlocspe+ opwointrvntn+ ventoverride+ useintakeright+ fanratedsones+ fanreducevnt+ bathroomfanses+ inletpullsoutdr+ inletlocation+ dessheatgainn+ dessheatgainne+ dessheatgaine+ dessheatgainse+ dessheatgains+ dessheatgainsw+ dessheatgainw+ dessheatgainnw+ maxheatgain+ deslheatgain+ destotlheatgainn+ destotlheatgainne+ destotlheatgainnw+ destotlheatgains+ destotlheatgainse+ destotlheatgainsw+ destotlheatgaine+ destotlheatgainw+ maxtotal+ desttotlheatloss+ mansmethod+ coolsizingpct+ coolingsizinglimit+ coolsizepctlimit+ heatsizinglimit+ heatsizepctlimit+ manuald+ designairflowcool+ hvacfanspdcool+ hvacfanspdheat+ designstaticp + designairflowheat + gfh + hrt;//+ equipment_id;
  llevo2=name+ radio1+ ventairrate+ venairflo0+ runtimecyc+ cycletimes;
  llevo3=specifiedsystype+ contlocspe+ opwointrvntn+ ventoverride+ useintakeright+ fanratedsones+ fanreducevnt+ bathroomfanses+ inletpullsoutdr+ inletlocation+ dessheatgainn+ dessheatgainne+ dessheatgaine+ dessheatgainse+ dessheatgains+ dessheatgainsw+ dessheatgainw+ dessheatgainnw+ maxheatgain+ deslheatgain+ destotlheatgainn+ destotlheatgainne+ destotlheatgainnw+ destotlheatgains+ destotlheatgainse+ destotlheatgainsw+ destotlheatgaine+ destotlheatgainw+ maxtotal+ desttotlheatloss+ mansmethod+ coolsizingpct+ coolingsizinglimit+ coolsizepctlimit+ heatsizinglimit+ heatsizepctlimit+ manuald+ designairflowcool+ hvacfanspdcool+ hvacfanspdheat+ designstaticp + designairflowheat + gfh + hrt;//+ equipment_id;
  
  porc=((req-llevo2)/total)*100;
  unit2=((llevo)/total)*100;
  falt = (((total)/total)*100)-porc-unit2;
if(falt>=1){$scope.comp = false;$scope.system.systemcomplete = false;}else{$scope.comp = true;$scope.system.systemcomplete = true;}
if($scope.comp != false){$scope.system.completesystems = true;}else{$scope.system.completesystems = false;};
  };
  $scope.reprog = function(){	
  };
  $scope.toogs=false;
  $scope.toog = function(){	
  if($scope.toogs == false){$scope.toogs = true;}else{$scope.toogs= false;};
  };
  
  
  $scope.room={
	  'hvac_design_id' : id,
	  'selection_id' : id_system
  };
	$scope.frma = function(data){	
	if((data.cycletimes)&&(data.runtimecyc)&&(data.venairflo0)&&(data.ventairrate)&&(data.radio1)&&(data.name)){
		
		$http.post("/rhAPI/update_designs2",data).then(function () {
			
        });
		//---***
		$http.get("/rhAPI/getSystemByDesign/"+$scope.select_design_id).then(function (response) {

			$rootScope.sys_r = response.data.system[0];
			$scope.room.selection_id=$rootScope.sys_r.selection_id;
			$scope.room.hvac_design_id=$scope.select_design_id;

        }).then(function () {
		});
		$http.get("/rhAPI/getRoom/"+$scope.room.selection_id).then(function (response) {

			$scope.sys_room = response.data.room;

        }).then(function () {
		});
		//---***

		$http.post("/rhAPI/systems",data).then(function () {
            
			$rootScope.msg='System created successfully.';
				$scope.data = data;
			$location.path('newSystem_dsg_room/' + $scope.select_design_id + '/' + $rootScope.sys_r.selection_id);
        });
		}else{};
	}
	$scope.edit_room={};
	$scope.editar = function(data){
	$scope.edit = false;
	$scope.edit_room=data;
	}
	$http.get("/rhAPI/view_system/"+$scope.select_system_id).then(function (response) {
		$scope.systemna = response.data.system[0];
	});
	$scope.editar2 = function(dataf,idda){
		$scope.edit = true;
		$http.post("/rhAPI/editRoom/"+idda,dataf).then(function () {
            		});
	}
	$scope.delRoom = function(idda,sy){
		$http.post("/rhAPI/deleteRoom/"+idda).then(function () {
            $location.path('newSystem_dsg_room/' + sy.hvac_design_id + '/' + sy.selection_id);
		});
		$http.get("/rhAPI/getRoom/"+$scope.room.selection_id).then(function (response) {
            
			$scope.sys_room = response.data.room;
        }).then(function () {
		});
	}
	$http.get("/rhAPI/getRoom/"+$scope.room.selection_id).then(function (response) {
            
			$scope.sys_room = response.data.room;
        }).then(function () {
		});
	$scope.frma2 = function(data){	
	if(($scope.room.room_descr)&&($scope.room.dsn_flow)){
		$http.post("/rhAPI/addRoom",data).then(function () {
			$scope.room.room_descr='';
			$scope.room.dsn_flow='';
		});}
		//---***
		$http.get("/rhAPI/getRoom/"+$scope.room.selection_id).then(function (response) {
            
			$scope.sys_room = response.data.room;
        }).then(function () {
		});
		//---***
			$rootScope.msg='System created successfully.';
			
			
		
	}
	
	$scope.termi = function(){	
	$rootScope.msg='System created successfully.';
	$location.path('home');
       }
	
});