'use strict';

angular.module('myApp.equipment', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/equipment', {
    templateUrl: 'equipment/equipment.html',
    controller: 'EquipmentCtrl'
  });

  $routeProvider.when('/view_equipment/:id', {
    templateUrl: 'equipment/view_equipment.html',
    controller: 'EquipmentCtrl'
  });
}])

.controller('EquipmentCtrl', function($scope ,$rootScope, $http, $routeParams, $location) {
	if(!$scope.contractor){
		
	};
	
    $rootScope.active = 'equipment';
	var id = $routeParams.id;
	
	$http.get("/rhAPI/getEquipments").then(function(response){
	$scope.equipments = response.data.equipments;
	var num=0;
	
	$scope.data.equipments = response.data.equipments;
    
  });
  
  $http.get("/rhAPI/getOneEquipment/"+id).then(function(response){
	$scope.equipment = response.data.Equipment[0];
  });
  $scope.delequip = function(){
  $http.post("/rhAPI/delete_equipment/"+id).
  then(function(response){
	  $rootScope.msg='equipment deleted successfully.';
	  $location.path('#/');
  });
  
  };
	
});